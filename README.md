# PostProcessor

## Requerimientos
* Endpoint de SQS.
* Nombre de tabla de DynamoDB.
* Node.js 6.10.
* IAM Role con:
    - AmazonSQSFullAccess
    - AmazonDynamoDBFullAccess
    - AWSLambdaExecute
    - CloudWatchEventsFullAccess (Para ejecutar Lambda)

