var aws = require('aws-sdk');
var queueUrl = 'URL_QUEUE_GOES_HERE';
var tableName = 'TABLE_NAME_GOES_HERE';
var sqs = new aws.SQS();
var docClient = new aws.DynamoDB.DocumentClient();

exports.handler = (event, context, callback) => {
    
    sqs.receiveMessage({
        QueueUrl: queueUrl,
        AttributeNames: ['All'],
        MessageAttributeNames: ['All'],
        MaxNumberOfMessages: '10',
        VisibilityTimeout: '10',
        WaitTimeSeconds: '0'
    }).promise()
        .then(data => {
            if (data.Messages) {
                           data.Messages.forEach(message => {
                                var attributes = message.MessageAttributes;
                                var command = attributes.command.StringValue;
                                
                                switch (command) {
                                    case 'Create':
                                        var putParams = {
                                            TableName: tableName,
                                            Item: {
                                                'key': attributes.key.StringValue,
                                                'sort': attributes.sort.StringValue,
                                                'useridfrom': attributes.useridfrom.StringValue,
                                                'useridto': attributes.useridto.StringValue,
                                                'body': attributes.body.StringValue
                                            }
                                        };
                                        
                                        docClient.put(putParams, (err, data) => {
                                            if (err) {
                                                console.log("Error while insering record to Dynamo debe with response: ", err);
                                            } else {
                                                console.log("Inserted record to DynamoDB with response: ", data);
                                            }
                                        });
                                        break;
                                        
                                    case 'Read':
                                        var readParams = {
                                            TableName: tableName,
                                            Key: {
                                                'key': attributes.key.StringValue,
                                                'sort': attributes.sort.StringValue
                                            }
                                        };
                                        docClient.get(readParams, (err, data) => {
                                                if (err) {
                                                    console.error("Unable to read item. Error JSON:", JSON.stringify(err, null, 2));
                                                } else {
                                                    console.log("GetItem succeeded:", JSON.stringify(data, null, 2));
                                                }
                                        });
                                        break;
                                        
                                    case 'Update':
                                        var updateParams = {
                                            TableName: tableName,
                                            Key: {
                                                'key': attributes.key.StringValue,
                                                'sort': attributes.sort.StringValue
                                            },
                                            UpdateExpression: "set body = :b ",
                                            ExpressionAttributeValues: {
                                                ":b": attributes.body.StringValue
                                            },
                                            ReturnValues: "UPDATED_NEW"
                                        };
                                        docClient.update(updateParams, (err, data) => {
                                                if (err) {
                                                    console.error("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
                                                } else {
                                                    console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
                                                }
                                        });
                                        break;
                                        
                                    case 'Delete':
                                        var deleteParams = {
                                            TableName: tableName,
                                            Key: {
                                                "key": attributes.key.StringValue,
                                                "sort": attributes.sort.StringValue
                                            }
                                        };
                                        docClient.delete(deleteParams, (err, data) => {
                                                if (err) {
                                                    console.error("Unable to delete item. Error JSON:", JSON.stringify(err, null, 2));
                                                } else {
                                                    console.log("DeleteItem succeeded:", JSON.stringify(data, null, 2));
                                                }
                                        })
                                        break;
                                        
                                }
                                
                                sqs.deleteMessage({
                                    QueueUrl: queueUrl,
                                    ReceiptHandle: message.ReceiptHandle
                                }).promise()
                                    .then(data => {
                                        console.log("Deleted message with ReceiptHandle: " + message.ReceiptHandle+ " from sqs");
                                    })
                                    .catch(err => {
                                        console.log("Error while deleting message with ReceiptHandle: " + message.ReceiptHandle+ " from sqs " +
                                        "Error: " + err);
                                    });
                
                            });
            }
        })
        .catch(err => {
            console.log("Error while fetching messages from sqs", err);
        });
    
    callback(null, 'Done executing Lambda');
};